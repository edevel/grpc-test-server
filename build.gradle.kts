import com.google.protobuf.gradle.*

plugins {
    application
    id("com.google.protobuf") version "0.8.10"
}

repositories {
    jcenter()
    mavenCentral()
    google()
}

application {
    mainClassName = "App"
}

val grpc_ver = "1.24.0"

dependencies {
    compile("com.google.protobuf:protobuf-java:3.6.1")
    compile("io.grpc:grpc-stub:${grpc_ver}")
    compile("io.grpc:grpc-protobuf:${grpc_ver}")
    compile("com.squareup.okhttp3:okhttp:4.2.1")
    runtime("io.grpc:grpc-netty-shaded:${grpc_ver}")
    
    compile("javax.annotation:javax.annotation-api:1.3.1")
    
    testCompile("junit:junit:4.12")
    testImplementation("io.grpc:grpc-testing:${grpc_ver}")
}

protobuf {
    protoc {
        // The artifact spec for the Protobuf Compiler
        artifact = "com.google.protobuf:protoc:3.6.1"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:${grpc_ver}"
        }
    }
    
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                id("grpc")
            }
        }
    }
}

sourceSets {
    main {
        java {
            srcDirs("build/generated/source/proto/main/grpc")
            srcDirs("build/generated/source/proto/main/java")
        }
    }
}
