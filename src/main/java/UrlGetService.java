import io.grpc.stub.StreamObserver;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import com.edevel.grpc.proto.*;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public final class UrlGetService extends GetUrlServiceGrpc.GetUrlServiceImplBase {

    static Logger logger = Logger.getLogger(UrlGetService.class.getName());

    int chunkSize = 10;
    OkHttpClient client = new OkHttpClient();

    @Override
    public void getUrl(GetUrlServiceOuterClass.UrlRequest request, StreamObserver<GetUrlServiceOuterClass.UrlResponse> responseObserver) {

        GetUrlServiceOuterClass.UrlResponse.Builder respBuilder = GetUrlServiceOuterClass.UrlResponse.newBuilder();

        Request req = new Request.Builder()
                .url(request.getUrl())
                .get()
                .build();
        try {
            Response httpResp = this.client.newCall(req).execute();

            // Processing HTTP headers.
            for(Map.Entry<String, List<String>> entry : httpResp.headers().toMultimap().entrySet()) {
                for(String value : entry.getValue()) {
                    respBuilder.addHeaders(
                            GetUrlServiceOuterClass.HttpHeader.newBuilder()
                                    .setHeaderName(entry.getKey())
                                    .setHeaderValue(value));
                }
            }
            responseObserver.onNext(respBuilder.build());

            // Processing HTTP body.
            InputStream bodyStream = httpResp.body().byteStream();
            byte[] chunkBuffer = new byte[chunkSize];
            int bytesRead = 0;
            while ((bytesRead = bodyStream.read(chunkBuffer)) != -1) {
                respBuilder = GetUrlServiceOuterClass.UrlResponse.newBuilder();
                respBuilder.setData(ByteString.copyFrom(chunkBuffer,0, bytesRead));
                responseObserver.onNext(respBuilder.build());
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, String.format("Error while getting URL %s: %s", request.getUrl(), e.getLocalizedMessage()));
        }

        responseObserver.onCompleted();
    }
}
