import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class App {

	static Logger logger = Logger.getLogger(App.class.getName());

	public static void main(String[] args) throws InterruptedException {
		try {
			AppServer appServer = new AppServer(8181);
			appServer.start();
			appServer.block();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while starting server! " + e.getLocalizedMessage());
		}
	}
}
