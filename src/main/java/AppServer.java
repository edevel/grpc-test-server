
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.grpc.*;
import okhttp3.OkHttpClient;

public final class AppServer {

    static Logger logger = Logger.getLogger(AppServer.class.getName());

    private Server server = null;
    private int port = 0;

    public AppServer(int port) throws IOException {
        this.port = port;
        this.server = ServerBuilder
                .forPort(port)
                .addService(new UrlGetService())
                .build();

    }
    public void start() throws IOException{
        if (server != null) {
            logger.log(Level.INFO, String.format("Starting server on port %d", this.port) );
            server.start();
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    AppServer.this.stop();
                }
            });
        }

    }

    public void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    public void block() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }
}